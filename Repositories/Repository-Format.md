Repositories have the following basic file structure:

```
./profiles/repo_name
./CATEGORY_NAME/metadata.yaml
./CATEGORY_NAME/MOD_NAME/MOD_NAME-VER.pybuild
./CATEGORY_NAME/MOD_NAME/MOD_NAME-OTHER_VER.pybuild
./CATEGORY_NAME/MOD_NAME/Manifest
./CATEGORY_NAME/MOD_NAME/metadata.yaml
```

## Categories
Any category must be listed in `profiles/categories` and contain a `metadata.yaml` file.

## Mods
Mod directories must be in a subdirectory of a category and their directory name
should be the same as the base name of the mod's pybuilds (excluding version).

The `Manifest` file is optional, but is required to contain a manifest entry for each source file listed in SRC_URI (i.e. only optional for pybuilds without sources).

`metadata.yaml` is optional.

## Profiles Directory
The files in profiles are optional, except for repo_name.

| File | Description |
| ---- | ----------- |
| arch.list | A newlline-separated list of architectures. An architecture may refer to a game-engine variant or an operating system, and is used to distinguish configurations where a package may be stable when used in the context of one, but unstable in the context of another. |
| categories | A newline-separated list of categories. These determine which directories in the root of the repository are considered categories containing packages. Directories not listed in this file will not be detected as containing packages. |
| license_groups.yaml | A yaml file containing a mapping from license groups to a whitespace-separated list of license names. Each group can be referenced within ACCEPT_LICENSE by prefixing it with an `@`, and they also reference each other using the same method. |
| package.mask | A [package mask](https://gitlab.com/portmod/portmod/-/wikis/Configuration/package-masking) file which applies regardless of profile |
| profiles.yaml | A yaml file containing profile declarations. See [Profiles](Repository/Profiles). |
| repo_name | A file containing a single line with the name of this repository |
| use.yaml | A file describing the global use flags, containing a mapping of use flag names to descriptions |
| use.alias.yaml | A file describing global use flags which have their values tied to packages. Contains a mapping of use flag names to package atoms. |
| desc | A directory containing USE_EXPAND descriptor files. See [USE_EXPAND](Repository/USE_EXPAND).

## Metadata Directory
The metadata directory is optional

| File | Description |
| ---- | ----------- |
| groups.yaml | Defines maintainer groups |
| news | See [GLEP 42](https://www.gentoo.org/glep/glep-0042.html), noting that news files are in yaml format rather than XML. Specification for the files can be found here: https://gitlab.com/portmod/portmod/-/blob/master/src/news.rs (TODO: Rustdoc), and the directory structure follows GLEP 42. |

## External Links
Gentoo Repository structure: https://devmanual.gentoo.org/general-concepts/tree/index.html

profiles directory in the Gentoo Developer Manual: https://devmanual.gentoo.org/profiles/index.html