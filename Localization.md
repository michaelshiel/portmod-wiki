Portmod's internationalization is handled using [Project Fluent](https://projectfluent.org).

Localization can be done using our Pontoon instance: https://portmod-l10n.herokuapp.com/projects/portmod/. A GitLab account is necessary for authentication, but no further signup is necessary. Just log-in with your GitLab account and start localizing.

If a language you would like to localize has not yet been set up in Pontoon, you can request that it be added by contacting the Portmod developers, either via [opening an issue](https://gitlab.com/portmod/portmod/-/issues), via [matrix](https://matrix.to/#/+portmod:matrix.org) or [email](~bmw/portmod@lists.sr.ht) (see the [README](https://gitlab.com/portmod/portmod) for up to date communication information).
