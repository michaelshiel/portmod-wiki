A maintainer is someone who takes the following responsabilities:

1. Updating the build files for a mod when the upstream mod updates
2. Responding to mod-specific issues, such as packaging bugs and compatibility issues

## Becoming a maintainer

The only requirement is that you need a gitlab account. Ideally you should also be
familiar with the mod and personally use it, but this is not required. Mod authors
maintaining their own mods is also ideal.

Fork the [OpenMW Mods](https://gitlab.com/portmod/openmw-mods/-/forks/new) project and add yourself to the maintainers field in `metadata.yaml` for
the mod in question. This should look something like

```yaml
maintainer: 
    email: foo@example.com
```

Note that the provided email must be the same as your primary gitlab email.

Then submit a merge request for your changes.
