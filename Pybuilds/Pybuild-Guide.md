This guide is designed to be a very brief guide to how to write pybuild files. It will not cover every single aspect of Portmod and the pybuild system.

To introduce a simple pybuild, here is `land-flora/bloated-morrowind/bloated-morrowind-1.0.pybuild`.
```python
# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild import Pybuild1, InstallDir, File


class Mod(Pybuild1):
    NAME = "Bloated Morrowind"
    DESC = "Adds around bloatspores to the wilderness of Morrowind"
    HOMEPAGE = "http://mw.modhistory.com/download-53-7817"
    LICENSE = "free-use"
    RDEPEND = "base/morrowind"
    KEYWORDS = "openmw"
    SRC_URI = "http://mw.modhistory.com/file.php?id=7817 -> bloatedmorrowind-1.0.zip"
    INSTALL_DIRS = [InstallDir("Data Files", PLUGINS=[File("Bloated Morrowind.esp")])]

```

## Basic Format
Note that a pybuild is basically just a python file. While any python code could be used, there is a special format necessary to work with the build system.

The file should start with a header comment as shown above. Following the header, any necessary imports should be included. For simple pybuilds such as this one, the `Pybuild1`, `InstallDir` and `File` imports should be all that are necessary.

## Variables
The most important part of the Pybuild is the class called `Mod`, deriving `Pybuild1` (there are other superclasses available through the `pyclass` directory). This Mod class should define a number of variables. The most commonly used variables are briefly described below, and details on optional variables can be found on the [Pybuild Format](Pybuild-Format) page.

The `NAME` is a more nicely formatted version of the name included in the build file's filename.

The `DESC` is a short description of what the mod does.

The `HOMEPAGE` is a link to the mod's website or homepage (including the http(s):// prefix).

The `LICENSE` indicates how the mod may be used. It can be either a particular license (e.g. GPL-3), or one of several general licenses such as, in this case, `free-use`, which indicates that the author has not released the mod under a specific license, but has indicated somewhere that the mod can be freely used for any purpose.

`RDEPEND` lists runtime dependencies. That is, other packages that need to be installed for this mod to work properly. Note that there is also a package for Morrowind.

`KEYWORDS` lists architectures (either openmw or tes3mp) that are known to work with this mod. A ~ in front of the name indicates that it is unstable, or has not been tested thoroughly.

The `SRC_URI` tells portmod where to find the source files. Here, the URL does not contain a filename, so we use arrow notation (->) to indicate what Portmod will name the downloaded file.

`INSTALL_DIRS` tells portmod how the archive is structured. In this case, the relevant data directory is in a subdirectory inside the archive called `Data Files`. This subdirectory also contains a plugin called `Bloated Morrowind.esp`, which is listed under `InstallDir.PLUGINS` so that it is automatically added to the content section of openmw.cfg. Other files within `Data Files` are automatically included in the final installed result, however files in the archive that are not within an InstallDir will not be installed.

## Generating Manifest

To generate the Manifest file that contains hashes of the source archives, use the `inquisitor manifest PATH_TO_FILE`. E.g. in the example above, `inquisitor manifest ./bloated-morrowind-1.0.pybuild`.

## Testing Pybuilds
The `inquisitor` command can also be used to check various attributes about a pybuild via the `scan` command (i.e. `inquisitor scan PATH_TO_FILE`). It will also scan the pybuild file if run from within the same directory (`inquisitor scan`) without passing the filename as an argument.

See also [Pybuild Guidelines and Tips](Pybuilds/Pybuild-Guidelines-and-Tips)
