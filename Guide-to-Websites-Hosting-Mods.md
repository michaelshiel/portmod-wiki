## GitLab and GitHub

https://gitlab.com and https://github.com

Grouped together as they behave very similarly

A significant advantage of both GitLab and GitHub is they provide standardized download locations. This allows packages to determine the URL based on the package version, as is encoded in the filename, allowing version information to be omitted from the package entirely, making it possible to update a package (as long as no significant changes to the archive structure have been made) by just copying and renaming the file.

E.g. from `modules/openmw-config::openmw`:

```python
from pybuild.info import P, PV, PN

class Package(Pybuild1):
    ...
    SRC_URI = f"https://gitlab.com/portmod/{PN}/-/archive/{PV}/{P}.tar.gz"
```

GitHub has a similar format, though github does not include the package name in the filename, so arrow notation is necessary to produce a proper filename.

E.g. from patches/omwllf::openmw

```python
from pybuild.info import P, PV

class Package(Pybuild1):
    ...
    SRC_URI = f"https://github.com/jmelesky/omwllf/archive/v{PV}.tar.gz -> {P}.tar.gz"
```

## Google Drive

Direct downloads are possible for files small enough that they are scanned by Google's antivirus. Large files are not scanned by antivirus and must be downloaded manually.

The download link that can be used for direct downloads is not the same as the one used for manual downloads, but the identifier can be extracted from the download page.

E.g. `https://drive.google.com/uc?id=17gqe2zdok6Or7-weAShTxyreHHRXwpyx&e=download` can be used to directly download tamriel-rebuilt, while the manual download page linked by tamriel-rebuilt.org is `https://drive.google.com/file/d/17gqe2zdok6Or7-weAShTxyreHHRXwpyx/view?usp=sharing`.

## ModDB

https://moddb.com

Downloads must be done manually. As very few mods from ModDB have yet been included in portmod repositories, this has so far been handled by each package individually.

## mw.moddinghall.com

https://mw.moddinghall.com

Note that the download section may not make it clear which version is which if there are multiple files with the same name.

The download URLs also include several unnecessary components. E.g. `https://mw.moddinghall.com/file/65-i-lava-good-mesh-replacer/?do=download&r=603&confirm=1&t=1&csrfKey=...`. Only the `?do=download` is required, and the `&r=`argument may or may not be present, depending on whether or not the mod has multiple files available to download (it's not clear if the links are broken if a mod adds a second file).

Thus for the above link, the following link works instead: `https://mw.moddinghall.com/file/65-i-lava-good-mesh-replacer/?do=download&r=603`

Since their download URLs don't include file names, arrow notation (e.g. `https:... -> file-name.zip` is necessary to ensure that the file name is meaningful and unique.

## mw.modhistory.com

https://mw.modhistory.com (as of 2021, it seems to have been rebranded as http://modhistory.com, though the original subdomain still works).

A relatively old site for morrowind mods, mw.modhistory has not had very much new content added in recent years. However it does provide many mods which are not available elsewhere. Their servers have a history of being unreliable, but seem to be getting better.

Their homepages are usually in the form `http://mw.modhistory.com/download--<id>`, and download links have the form `http://mw.modhistory.com/file.php?id=<id>`. Note that the homepages may contain a number between the hyphens. E.g. `http://mw.modhistory.com/download-56-<id>`, but this appears to be just a counter and can be safely discarded.

Since their download URLs don't include file names, arrow notation (e.g. `https:... -> file-name.zip` is necessary to ensure that the file name is meaningful and unique.

## NexusMods

https://nexusmods.com

Does not allow direct downloads of files without the use of their premium API (which portmod does not yet support). A special class called `NexusMod`, in `common.nexus` (in the `openmw`, `x3` and `bsa` (used by `oblivion` and `fallout-nv`) repositories) has been written to more nicely handle manual downloads from nexusmods.

Two interfaces are supported.

1. `NEXUS_URL`: This specifies the homepage of the mod to be downloaded. It is assumed that all files are from this page. Files should be included in `SRC_URI` as plain filenames, and they must be identical names to the ones provided on the site (save for spaces, [which get turned into underscores](Pybuilds/Pybuild-Guidelines-and-Tips#source-files-which-contain-spaces-in-their-name)), which makes sure portmod can find the files the user has manually downloaded without the user having to rename them.
2. `NEXUS_SRC_URI`: This is more verbose, but allows you to specify files from multiple nexus pages in the same package, while indicating to the user precisely where to find the files. For each file, you should specify the download page for the file (not the mod page), and use arrow notation to indicate the filename. E.g. `NEXUS_SRC_URI = "https://www.nexusmods.com/morrowind/mods/48628?tab=files&file_id=1000024872 -> MCAR-48628-4-0-1621574799.7z"`

Both `NEXUS_URL` and `NEXUS_SRC_URI` can contain [use-conditionals](pybuilds/use-flags).