Portmod has two different levels of configuration: Prefixes, and profiles.

Prefixes are user-created and allow multiple configurations to exist side-by-side on a system.

Profiles are defined by the repository, and provide default settings for a prefix.

## Creating a prefix

You can create a prefix using the command `portmod init <prefix> <arch>`.

`<prefix>` is the name of the prefix you want to create. This name can be arbitrary, but it is recommended, unless you are intending on setting up multiple prefixes for the same architecture, to use the architecture as the prefix name (e.g. `openmw`). 

`<arch>` is the architecture of the prefix (i.e. the game engine). Known supported architectures are listed in [profiles/arch.list](https://gitlab.com/portmod/meta/-/blob/master/profiles/arch.list), but you may want to consult the specific package repository you are using.

Supported architectures (listed here for convenience):
- openmw
- x3ap
- gzdoom
- tes3mp (support is currently very limited)

Unless you pass the `--no-confirm` argument to `portmod init`, it will prompt you to select package repositories and a profile.

Optionally, you can include a directory as a third argument to `portmod init`. Doing so will install the prefix into that directory, something which is necessary for certain DRM systems where the game can only be launched if the files are in a certain place (and also removes the need to copy source files for engines which don't support any sort of virtual file system). The proprietary game engines supported by portmod use this method (see documentation for your architecture).

### Add and Synchronize Package repositories

You can use the command `portmod <prefix> select repo list` to list available repositories, and add them using `portmod <prefix> select repo add <repo>`, where repo is either the name of the repo, or the number in the list.

This will both initialize and synchronize the selected repository, and add it to the REPOS variable in your prefix's portmod.conf. Repositories are shared between prefixes, but only those listed in REPOS will be used.

### Select a Profile

The profile provides default settings that are tailored for your game setup. See [Profiles](profiles) for details about the different profile options (note: only applies to `openmw`/`tes3mp`).

In addition to during prefix initialization, you can use `portmod <prefix> select profile list` to see a list of available profiles, and  `portmod <prefix> select profile set NUM` can be used to select the profile you want.

**NOTE: the `default/openmw/1.0` profiles are deprecated and only meant to be used with the portmod 2.0_rc9 or earlier. They will be removed eventually**

Once you've chosen a profile, you should run a global update to install any mods required by your profile. You can do this using `portmod <prefix> merge --update --deep --newuse @world` (or `portmod <prefix> merge -uDN @world`).

### (Optional) Creating a Prefix Alias

*For users of shells other than bash, you will need to look elsewhere for this information, and note that other unix shells have similar features, though the syntax may vary. Consult your shell's documentation for information on how to create aliases.*

If you use the bash shell, you can create an alias for prefix-related commands by adding `alias <alias_name>="portmod <prefix>"`.

For example, for an `openmw` prefix, you could do the following:

```bash
alias omw="portmod openmw"
```

This would allow you to use the command `omw merge` in place of `portmod openmw merge`.

You could even create an alias for each of the subcommands ("merge", "select", "search", "query", "use"). E.g. `alias omwmerge="portmod openmw merge"`.

Note that the documentation on this wiki will not assume that you have created an alias, and will always give the command in full.

## Migrating Manual Setups (Optional)
You can use the portmod-migrate tool to import archives you've already downloaded, as well as to detect mods that you have installed manually. portmod-migrate can be installed using pip similarly to portmod.

https://gitlab.com/portmod/portmod-migrate

Any mods which aren't yet supported can be included as [Local Mods](Configuration/Local-Mods)