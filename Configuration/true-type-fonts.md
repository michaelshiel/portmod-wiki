True Type Fonts can be configured using the `modules/openmw-fonts` package. This will automatically generate the `openmw_font.xml` file (briefly mentioned in the [openmw documentation](https://openmw.readthedocs.io/en/stable/reference/modding/font.html)) using fonts you select through the interface.

Three font types are used by openmw:

- `daedric` - Used for displaying daedric text.
- `magic` - Used for most interface text.
- `mono` - Used for the console.

Configuration in general is done using the `portmod <prefix> select fonts` interface. You can display available fonts using the command:

```
portmod <prefix> select fonts list {type}
```

where `type` is one of the types listed above, and set fonts using the command:

```
portmod <prefix> select fonts set {type} {value}
```

where value is either the name of the font, or its index in the list.

Fonts from the `media-fonts` category in the repository will appear in the list, as well as system fonts on systems with [Fontconfig](https://www.freedesktop.org/wiki/Software/fontconfig/) (except for Daedric, which is assumed will only come from the repo). Note that fonts are filtered by type to avoid displaying too many unnecessary fonts.
