The repos.cfg file in the portmod config directory (See [Portmod Config](portmod-config))
can be used to specify the various repositories that Portmod pulls information from. The
following entry is for the default repository and is automatically included if the
`repos.cfg` file does not exist.

```ini
[openmw]
location = ${PORTMOD_LOCAL_DIR}/openmw
auto_sync = True
sync_type = git
sync_uri = https://gitlab.com/portmod/openmw-mods.git
priority = -1000
```

## Local Repository
A local repository can be specified by setting `auto_sync = False` (or omitting it, as it
is false by default).

E.g. to create a custom repository where you can add your own pybuilds, add the following
entry to repos.cfg.

```ini
[user]
location = /path/to/repo
auto_sync = False
priority = 0
```

You also likely want to create the file `/metadata/layout.conf` within the repository, and specify the option `masters = "openmw"`. This makes your custom repo inherit metadata
from the openmw repo, such as categories and global use flags. If you omit this then portmod
won't be able to find your mods without you ensuring that their category exists in the
`profiles/categories` file in your repo.

`metadata/layout.conf`:
```ini
masters = "openmw"
```

The priority needs to be higher than the priority of the default repository if you have mods
with the same name as those in the main repo, as if a mod is otherwise identical to the loader
(i.e. the mods have the same name and version) Portmod will attempt to load the mod in the
higher priority repo.

The only required file that must exist in a repo is `profiles/repo_name`, which should
include a single line containing the name of your repository.

## External Resources
repos.conf on the Gentoo Wiki: https://wiki.gentoo.org/wiki//etc/portage/repos.conf
(Note that only the options shown in the above examples have been implemented in Portmod)
