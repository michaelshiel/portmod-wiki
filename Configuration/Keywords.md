Most packages declare one of the following engine-specific keywords in their KEYWORDS field:

1. Stable: `arch` (`openmw`, `tes3mp`) - This version of the mod and the pybuild are tested and not known to have any serious issues with the given platform.
2. Testing: `~arch` (`~openmw`, `~tes3mp`) - The version of the mod and the pybuild are believed to work and do not have any known serious bugs, but more testing should be performed before being considered stable.
3. No keyword: If a package has no keyword for a given arch, it means it is not known whether the package will work, or that insufficient testing has occurred for ~arch. 
4. Masked: `-arch` (`-openmw`, `-tes3mp`) The package version will not work on the arch. This likely means it relies on a feature not supported by the engine, or contains serious bugs that make it unusable.

By default, only stable versions of packages are installed. For unstable versions you will need to accept the keyword.

You can enable testing packages by default by overriding the default `ACCEPT_KEYWORDS` in portmod.conf with the testing keyword appropriate for your engine.

You can accept keywords for specific packages by adding the mod version and keyword to `PORTMOD_CONFIG_DIR/mod.accept_keywords`. E.g:

```
=base/patch-for-purists-3.2.1 ~openmw
>=base/patch-for-purists-3.1.3 ** # To ignore keywords and make the package visible regardless of keywords
```

### Important Note:
Many packages have been added to portmod and marked as stable without significant testing, mainly to increase the usability of the system and avoid diving too deeply into the underdeveloped keywording feature. It will take some time for the repository to truly stabilize.

## External References

https://wiki.gentoo.org/wiki/ACCEPT_KEYWORDS

https://wiki.gentoo.org/wiki//etc/portage/package.accept_keywords

https://wiki.gentoo.org/wiki/Knowledge_Base:Accepting_a_keyword_for_a_single_package