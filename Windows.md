This page is for Windows-specific caveats and things to take note of. For Windows-specific installation information, see [Installation on Windows](Installation/Installation-On-Windows).

## Powershell

The `@` symbol has a special meaning in Powershell. As such, commands such as `portmod <prefix> merge -uDN @world` will not work as expected when run in powershell. This can be worked around by escaping the `@` symbol using the ``` ` ``` escape character. E.g. ```portmod <prefix> merge -uDN `@world```
