When installing mods using the `portmod <prefix> merge` command, the following operations are performed, in order. Note that various arguments (see `portmod <prefix> merge --help`) may modify this behaviour.

## 1.  Calculate dependencies
Dependencies are specified in pybuild files (see [DEPEND and RDEPEND](Pybuilds/Pybuild-Format#rdepend-and-depend)).

A dependency graph is created and turned into a list of mods to install using a topological sorting algorithm. Mods are always installed before their build dependencies (DEPEND), and an attempt is made to ensure they are also installed before their runtime dependencies (RDEPEND), however this may not be possible. As such, interrupting installation of mods may leave the mod setup in an invalid state due to missing runtime dependencies.

## 2.  Prompt user to install

Displays the list of changes to be performed and prompts the user.

This includes both mods to be installed or removed, as well as any configuration changes that Portmod has determined are necessary for the desired operation to be performed.

Also lists any files that need to be manually downloaded, and these must be downloaded and moved into the cache directory prior to accepting the prompt.

## 3.  Apply any automatically determined configuration changes

The dependency resolution phase also ensures that mod configuration dependencies are satisfied. Some mods may depend on configuration options (e.g. USE flags), and the dependency calculator will attempt to resolve these. If there are any such changes necessary, they will be automatically applied to the user's configuration (they were displayed to the user prior to the prompt in step 2).

## 4.  Install each mod
1. Download source archives
2. Extract archives to temporary directory
3. Prepare contents of archives (patches, configuration etc.)
4. Install contents to temporary directory
  (i.e. create installed structure, there may be one or more directories in the previous stage that get combined here).
5. Move installed files to final destination in the mod directory
6. Add metadata about installed mod to db
7. Clean up temporary installation files (debug mode skips this)
8. Sort config files (hiding warnings)
   Mods may depend on the openmw vfs when installing, so we need to ensure that it always reflects the installed mods.
  This is skipped on the last mod installed, as we will perform it again in step 7.
## 5.  Commit changes to the db
Portmod stores metadata about installed mods in `${PORTMOD_LOCAL_DIR}/db`, with subdirectories of the form `${CATEGORY}/${MN}`. This directory is also a git repository. Files are added as each mod is installed, but the entire set of changes are only committed at the end.
## 6.  Housekeeping
Portmod has a rebuild tracking feature that can be used for metamods that collect information from other mods. See [REBUILD](Pybuilds/Pybuild-Format#rebuild) for details.
## 7.  Sort config files.
Finally, the openmw config files are sorted again, and warnings to the user about extraneous entries, i.e. entries in the config that Portmod did not create. These cannot be tracked automatically unless you create [user rules for them](Configuration/User-Sorting-Rules)