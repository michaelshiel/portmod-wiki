Profiles are used to configure static information about your installation.

### Architecture
A required component of a profile is the related architecture, which determines the keyword used when determining the stability of mods, as well as any architecture-specific options.

Current architectures in the main repo are:
- openmw
- tes3mp

### Version
Profiles include a numeric version component. Generally you should choose the newest version available, as older versions are generally maintained to avoid breaking existing installations.

### Other options
Note that for the morrowind profiles, you may want to set `MORROWIND_PATH` in [portmod.conf](portmod-config) to point to your morrowind installation. If this variable is not set, the build file will try to automatically detect where morrowind is installed.

#### morrowind
Indicates that you have the Morrowind data files without expansion packs already installed on your system.

#### morrowind-tb
Indicates that you have the Morrowind data files with the Tribunal expansion pack already installed on your system.

#### morrowind-bm
Indicates that you have the Morrowind data files with the Bloodmoon expansion pack already installed on your system.

#### morrowind-tb-bm
Indicates that you have the Morrowind data files with the Tribunal and Bloodmoon expansion packs already installed on your system.

## External Resources
https://wiki.gentoo.org/wiki/Profile_(Portage) - Detailed information on the profile structure