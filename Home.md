[Technical Design Notes](Technical Design Notes)

Also see https://portmod.gitlab.io/portmod/ for the API documentation

To modify this wiki, you can make a merge request to https://gitlab.com/portmod/portmod-wiki

## Guide
[Installation](Installation/Installation)

[Setup](configuration/Setup)

[Getting Started](getting-started)

## Configuration

[Profiles](profiles)

[Portmod Config File](Configuration/portmod-config)

[Repos.cfg](Configuration/repos.cfg)

[Keywords](Configuration/keywords)

[Sets](Configuration/sets)

[Package Masking](Configuration/package-masking)

[True Type Fonts](Configuration/true-type-fonts)

[Use Flags](Configuration/use-flags)

[Local Mods](Configuration/Local-Mods)

## Architectures

The following architectures are currently supported.

Contributions via merge request are always welcome. If you've created a package repository and would like it added to the official list, create a merge request on the [meta](https://gitlab.com/portmod/meta) repository and add it to `metadata/repos.cfg`.

### OpenMW (openmw)

Original and largest package repository for portmod.

https://gitlab.com/portmod/openmw-mods.git

See [OpenMW](OpenMW/OpenMW)

### GZDoom (gzdoom)

https://gitlab.com/portmod/zdoom.git

### X3: Albion Prelude (x3ap)

https://gitlab.com/portmod/x3.git

### TES3MP (tes3mp)

Support for server-side scripts is available via the tes3mp-server repository: https://gitlab.com/portmod/tes3mp-server.git

Client-side mods can be installed through the openmw repository (https://gitlab.com/portmod/openmw-mods.git). Note that while a tes3mp architecture also exists in that repository, it is mostly untested. In general, packages in the `assets-*` categories *should* be functional (though few if any have been marked as stable on `tes3mp`).

### The Elder Scrolls IV: Oblivion (oblivion)

**Highly Experimental. See README and #209**

https://gitlab.com/portmod/oblivion.git

### Fallout: New Vegas (fallout-nv)

**Highly Experimental. See README and #209**

https://gitlab.com/portmod/fallout-nv.git

### Kerbal Space Program (ksp)

**Unstable, but functional. See #229 for details on the current status**

- https://gitlab.com/portmod/ksp
- https://gitlab.com/portmod/ckan

## Development

### Pybuilds

[Guide to writing Pybuild files](pybuilds/pybuild-guide)

[Pybuild Format](pybuilds/Pybuild-Format) - A more detailed overview of the pybuild format.

[Pybuild Guidelines and Tips](pybuilds/Pybuild-Guidelines-and-Tips) - Guidelines that should be followed when writing Pybuilds.

[Manifest](Manifest)

[Mod and Category Metadata](Metadata)

[Use Flags](pybuilds/use-flags)

[Pybuild Sandbox](Sandbox)

### Repositories

[Repository Format](Repositories/Repository-Format)

[Profiles](Repositories/Profiles)

### Contributing

[Mod Maintainers](Mod-Maintainers)

[Development](Development)

[Localization](Localization)

[Guide to Websites Hosting Mods](Guide to Websites Hosting Mods)

### Modules

[Modules](Modules)

## Usage Notes

[Windows Issues](Windows)

- Make sure the openmw-launcher is not running. If you install mods when the openmw-launcher is running the mods will be removed from the config file when the openmw-launcher is closed.
- When installing mods, relevant plugins are enabled automatically. There is no need to enable plugins using openmw-launcher.
- In general, manual configuration is not recommended when using Portmod as it may lead to undesired behaviour, notably, the potential loss of your changes when running updates. If there is a configuration step that a pybuild omits you have the following options:
    1. Report the issue and wait for a fix
    2. Fix the pybuild yourself:
        - Create a local repository and add it to repos.cfg in the config directory
        - Copy and modify the offending pybuild.
        - Once you've added and tested the fix, please submit the changes to the repository so that others can benefit from the change.