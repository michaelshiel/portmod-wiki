Original and most active package repository for portmod.

https://gitlab.com/portmod/openmw-mods.git

[Prefix Layout](OpenMW/Prefix-Layout)

[User Sorting Rules](OpenMW/user-sorting-rules)

#### bsatool
Portmod's support for openmw depends on `bsatool` (for reading the VFS archives), which is usually packaged with OpenMW.

On certain platforms it may be optional:
- Gentoo: Available with `games-engines/openmw[devtools]`
